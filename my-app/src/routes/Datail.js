import {useParams} from 'react-router-dom';
import {useState , useEffect} from "react";
function Detail(){
    const {id}= useParams();
    const [loading,setLoading] = useState(false);
    const [detail, setDetail] = useState({});
    const getMovie = async ()=>{
        const json = await( 
                        await fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${id}`)
                    ).json();
                    
                    setLoading(true);
                    setDetail(json.data.movie);
                    
    }
    console.log("영화 상세" , detail);
    useEffect(()=>{
      getMovie();
    },[]);
    return  <div>{detail.title}</div>
}
export default Detail