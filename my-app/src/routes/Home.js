import {useState , useEffect} from "react";
import Movie from '../components/Movie.js'
import propTypes from 'prop-types';
function Home(){

    const [loading ,setLoading ] = useState(true);
    const [movies ,setMovies] =useState([]);
    const getMovies = async ()=>{
        const json  = await (
          await fetch("https://yts.mx/api/v2/list_movies.json?minimum_rating=8.8&sort_by=year")
        ).json();
        setMovies(json.data.movies);
        setLoading(false);
    }
    useEffect(()=>{
      getMovies();
    },[]);
    console.log("movies:: " , movies);
    return (
      <div>
        <h1>The Movies! {loading?"": `(${movies.length})`}</h1>
        <div>
          {loading?<strong>Loading</strong>
          :(
            <div>
              {movies.map((movie) =><Movie key={movie.id} medium_cover_image={movie.medium_cover_image} 
              title={movie.title} summary={movie.summary} genres={movie.genres} id={movie.id}/>)}
          </div>
          )}
        </div>
      </div>
    );
  }
  
  Movie.propTypes ={
    medium_cover_image: propTypes.string.isRequired,
    title : propTypes.string.isRequired,
    summary:propTypes.string.isRequired,
    genres:propTypes.arrayOf(propTypes.string).isRequired,
    id:propTypes.number.isRequired
  }


export default Home;