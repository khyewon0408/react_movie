import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import Home from './routes/Home.js';
import Detail  from './routes/Datail.js';
function App() {
 
  return <Router>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route  path="/movie/:id" element={<Detail />}/>
    </Routes>
  </Router>;
}
   
export default App;
